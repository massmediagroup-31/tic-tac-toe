<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GameField
 * @package App
 */
class GameField extends Model
{
    protected $fillable = ['field'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo('App\Game');
    }
}
