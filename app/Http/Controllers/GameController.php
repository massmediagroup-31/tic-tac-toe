<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePlayerRequest;
use App\Services\GameFieldService;
use App\Services\GameService;
use App\Game;
use Illuminate\Http\Request;

/**
 * Class GameController
 * @package App\Http\Controllers
 */
class GameController extends Controller
{
    protected $gameService;
    protected $game;
    protected $gameFieldService;

    /**
     * GameController constructor.
     * @param GameService $gameService
     * @param Game $game
     * @param GameFieldService $gameFieldService
     */
    public function __construct(GameService $gameService, Game $game, GameFieldService $gameFieldService)
    {
        $this->gameService = $gameService;
        $this->game = $game;
        $this->gameFieldService = $gameFieldService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createPlayers()
    {
        return view('game-fields.create-players');
    }

    /**
     * @param StorePlayerRequest $playerRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fillField(StorePlayerRequest $playerRequest)
    {
        $player1 = $playerRequest->get('player1');
        $player2 = $playerRequest->get('player2');

        $gameId = $this->gameService->startGame(['player1' => $player1, 'player2' => $player2]);

        return redirect()->route('gameProcess', ['id' => $gameId]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function gameProcess(int $id)
    {
        $game = $this->game->find($id);

        $players = json_decode($game->players);
        $gameField = json_decode($game->gameField->field);

        return view('game-fields.game-process', compact('game', 'players', 'gameField'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function saveMove(Request $request)
    {
        $gameId = $request->get('gameId');
        $gameFieldForm = $request->get('fields');

        $game = $this->game->find($gameId);
        $gameFieldDb = json_decode($game->gameField->field);

        // $coordinate[0] = x, $coordinate[1] = y
        $coordinate = [];
        for ($x = 0; $x < count($gameFieldForm); $x++) {
            for ($y = 0; $y < count($gameFieldForm); $y++) {
                if (isset($gameFieldForm[$x][$y])) {
                    // save x,y if coordinate is free
                    if ($gameFieldForm[$x][$y] == $game->last_player_marker && !isset($gameFieldDb[$x][$y])) {
                        $coordinate[] = $x;
                        $coordinate[] = $y;
                    }

                    if ($gameFieldForm[$x][$y] != $game->last_player_marker) {
                        if (!isset($gameFieldDb[$x][$y])) {
                            return redirect()->back()
                                ->withErrors(['Wrong insert, you need insert  - '.$game->last_player_marker]);
                        }
                    }

                    // cannot add to exists coordinate
                    if (isset($gameFieldDb[$x][$y])) {
                        if ($gameFieldForm[$x][$y] !== $gameFieldDb[$x][$y]) {
                            return redirect()->back()->withErrors(['Coordinate is already taken']);
                        }
                    }

                    if (count($coordinate) > 3) {
                        return redirect()->back()->withErrors(['You need only one move']);
                    }

                    // if delete exists marker
                } else {
                    if (isset($gameFieldDb[$x][$y])) {
                        return redirect()->back()->withErrors(['Coordinate is already taken']);
                    }
                }
            }
        }

        if (!count($coordinate)) {
            return redirect()->back()->withErrors(['You need make move']);
        }

        // send x,y to save
        $updatedField = $this->gameService->saveMove($gameId, $coordinate[0], $coordinate[1]);
        if ($this->gameFieldService->checkWinnerByMark($updatedField, $game->last_player_marker)) {
            $players = json_decode($game->players);
            $winner = $game->last_player_marker == 'o' ? $players->player1 : $players->player2;
            $marker = $game->last_player_marker;

            return view('game-fields.winner', compact('winner', 'marker', 'updatedField'));
        }

        if ($this->gameFieldService->checkDraw($updatedField)) {
            return view('game-fields.draw', compact('updatedField'));
        }

        return redirect()->back();
    }
}
