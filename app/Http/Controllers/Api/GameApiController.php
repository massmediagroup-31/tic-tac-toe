<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\GameFieldService;
use Illuminate\Http\Request;
use App\Services\GameService;
use App\Game;

/**
 * Class GameApiController
 * @package App\Http\Controllers\Api
 */
class GameApiController extends Controller
{
    protected $gameService;
    protected $game;
    protected $gameFieldService;

    /**
     * GameApiController constructor.
     * @param GameService $gameService
     * @param Game $game
     * @param GameFieldService $gameFieldService
     */
    public function __construct(GameService $gameService, Game $game, GameFieldService $gameFieldService)
    {
        $this->gameService = $gameService;
        $this->game = $game;
        $this->gameFieldService = $gameFieldService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fillField(Request $request)
    {
        $player1 = $request->get('player1');
        $player2 = $request->get('player2');

        if (!$player1) {
            return response()->json('Enter player 1 name');
        }
        if (!$player2) {
            return response()->json('Enter player 2 name');
        }

        $gameId = $this->gameService->startGame(['player1' => $player1, 'player2' => $player2]);

        return response()->json(['game id' =>$gameId]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function gameProcess(int $id)
    {
        $game = $this->game->find($id);

        $players = json_decode($game->players);
        $gameField = json_decode($game->gameField->field);

        return response()->json(['game id' => $game, 'Players' => $players, 'Game Field' => $gameField]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveMove(Request $request)
    {
        $gameId = $request->get('gameId');

        $x = (int)$request->get('x');
        $y = (int)$request->get('y');

        if ($x > 2 || $x < 0 || $y > 2 || $y < 0) {
            return response()->json(['Wrong coordinate']);
        }

        $game = $this->game->find($gameId);

        $gameField = $game->gameField->field;

        $gameFieldArr = json_decode($gameField);

        if (isset($gameFieldArr[$x][$y])) {
            return response()->json(['Coordinate is exist', 'game' => $gameField]);
        }

        $updatedField = $this->gameService->saveMove($gameId, $x, $y);
        if ($this->gameFieldService->checkWinnerByMark($updatedField, $game->last_player_marker)) {
            $players = json_decode($game->players);
            $winner = $game->last_player_marker == 'o' ? $players->player1 : $players->player2;
            $marker = $game->last_player_marker;

            return response()->json(['Winner player' => $winner, 'winner marker' => $marker, 'field' => $updatedField]);
        }

        if ($this->gameFieldService->checkDraw($updatedField)) {
            return response()->json(['result game' => 'Draw', 'field' => $updatedField]);
        }

        return response()->json(['game' => $updatedField, 'Player market' => $game->last_player_marker]);
    }
}
