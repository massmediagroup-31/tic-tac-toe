<?php

namespace App\Services;

use App\GameField;

/**
 * Class GameFieldService
 * @package App\Services
 */
class GameFieldService
{
    /**
     * @const array GAME_FIELD
     */
    const GAME_FIELD = [
        [null, null, null],
        [null, null, null],
        [null, null, null],
    ];

    /**
     * @var GameField
     */
    private $gameField;

    /**
     * GameFieldService constructor.
     * @param GameField $gameField
     */
    public function __construct(GameField $gameField)
    {
        $this->gameField = $gameField;
    }

    /**
     * @return int
     */
    public function generateGameField(): int
    {
        $this->gameField->field = json_encode(self::GAME_FIELD);

        $this->gameField->save();

        return $this->gameField->id;
    }

    /**
     * @param int $id
     * @param int $x
     * @param int $y
     * @param string $marker
     * @return array
     */
    public function updateField(int $id, int $x, int $y, string $marker): array
    {
        $gameField = $this->gameField->find($id);
        $field = json_decode($gameField->field);
        $field[$x][$y] = $marker;

        $gameField->update(['field' => json_encode($field)]);

        return $field;
    }

    /**
     * @param array $field
     * @param string $mark
     * @return bool
     */
    public function checkWinnerByMark(array $field, string $mark): bool
    {
        $winRow = array_fill(0, 3, $mark);
        for ($i = 0; $i < 3; $i++) {
            if ($field[$i] === $winRow) {
                return true;
            }
        }
        for ($i = 0; $i < 3; $i++) {
            if ([$field[0][$i], $field[1][$i], $field[2][$i]] === $winRow) {
                return true;
            }
        }
        return [$field[0][0], $field[1][1], $field[2][2]] === $winRow ||
            [$field[2][0], $field[1][1], $field[0][2]] === $winRow;
    }

    /**
     * @param array $field
     * @return bool
     */
    public function checkDraw(array $field): bool
    {
        foreach ($field as $row) {
            foreach ($row as $cell) {
                if (!isset($cell)) {
                    return false;
                }
            }
        }

        return true;
    }
}
