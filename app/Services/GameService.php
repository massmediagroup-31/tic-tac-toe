<?php

namespace App\Services;

use App\Game;

/**
 * Class GameService
 * @package App\Services
 */
class GameService
{
    /**
     * @var GameFieldService
     */
    private $gameField;
    /**
     * @var Game
     */
    private $game;

    /**
     * GameService constructor.
     * @param GameFieldService $gameField
     * @param Game $game
     */
    public function __construct(GameFieldService $gameField, Game $game)
    {
        $this->gameField = $gameField;
        $this->game = $game;
    }

    /**
     * @param array $players
     * @return int
     */
    public function startGame(array $players): int
    {
        $gameFieldId = $this->gameField->generateGameField();

        $this->game->game_field_id = $gameFieldId;
        $this->game->players = json_encode($players);
        $this->game->save();

        return $this->game->id;
    }

    /**
     * @param string $marker
     * @return string
     */
    protected function getMarker(string $marker): string
    {
        return $marker == 'o' ? 'x' : 'o';
    }

    /**
     * @param int $gameId
     * @param int $x
     * @param int $y
     * @return array
     */
    public function saveMove(int $gameId, int $x, int $y): array
    {
        $game = $this->game->find($gameId);
        $fieldId = $game->gameField->id;

        $updatedField = $this->gameField->updateField($fieldId, $x, $y, $game->last_player_marker);

        $game->last_player_marker = $this->getMarker($game->last_player_marker);
        $game->save();

        $this->gameField->checkWinnerByMark($updatedField, $game->last_player_marker);

        return $updatedField;
    }
}
