@extends('layouts.app')

@section('content')

    <h4>Winner is <span style="color: #1f6fb2">{{ $winner }}</span> by <span style="color: #1f6fb2">{{ $marker }}</span></h4>


    {!! Form::open(array( 'route' => ['saveMove'], 'method' => 'POST') ) !!}

    @for($x = 0; $x < 3; $x++)
        @for($y = 0; $y < 3; $y++)
            {{ Form::text("fields[$x][$y]", $updatedField[$x][$y] ) }}
        @endfor
        <br>
    @endfor

    {!! Form::close() !!}

@endsection