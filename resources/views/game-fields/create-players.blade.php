@extends('layouts.app')

@section('content')

    @if (count($errors))
        @component('layouts.components.alert')
        @endcomponent
    @endif

    <h4>Enter players name:</h4>

    {!! Form::open(array( 'route' => ['fillField'], 'method' => 'POST') ) !!}

    {{ Form::text('player1', null, ['class' => 'form-control']) }}
    {{ Form::text('player2', null, ['class' => 'form-control']) }}


    {{ Form::submit('Send', ['class' => 'btn btn-info pull-right'] )}}

    {!! Form::close() !!}

@endsection