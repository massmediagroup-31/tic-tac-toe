@extends('layouts.app')

@section('content')

    {{ $player1 ?? 'not pl1'}}
    {{ $player2 ?? 'not pl2'}}
    @if(isset($player1) && isset($player2))

        @if (count($errors))
            @component('layouts.components.alert')
            @endcomponent
        @endif

        {!! Form::open(array( 'route' => 'fillField', 'method' => 'POST') ) !!}

        @for($x = 0; $x < 3; $x++)
            @for($y = 0; $y < 3; $y++)
                {{ Form::text("$x$y", null) }}
            @endfor
            <br>
        @endfor

        <input name="player1" value="{{ $player1 }}" hidden>
        <input name="player2" value="{{ $player2 }}" hidden>

        {{ Form::submit('Send', ['class' => 'btn btn-info pull-right'] )}}

        {!! Form::close() !!}

    @endif
@endsection

