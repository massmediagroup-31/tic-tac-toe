@extends('layouts.app')

@section('content')

    <h4>
        Turn player: <span style="color: #1f6fb2">{{ $game->last_player_marker=='o' ? $players->player1 : $players->player2 }} </span>,
        go by: <span style="color: #1f6fb2">{{ $game->last_player_marker }}</span>
    </h4>

    @if (count($errors))
        @component('layouts.components.alert')
        @endcomponent
    @endif

    {!! Form::open(array( 'route' => ['saveMove'], 'method' => 'POST') ) !!}

    @for($x = 0; $x < 3; $x++)
        @for($y = 0; $y < 3; $y++)
            {{ Form::text("fields[$x][$y]", $gameField[$x][$y] ) }}

        @endfor
        <br>
    @endfor
    <input name="gameId" value="{{ $game->id }}" hidden>
    {{ Form::submit('Send', ['class' => 'btn btn-info pull-right'] )}}

    {!! Form::close() !!}

@endsection