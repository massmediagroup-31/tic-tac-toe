<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create-players', 'GameController@createPlayers')->name('createPlayers');

Route::post('/field', 'GameController@fillField')->name('fillField');
Route::get('/game-process/{id}', 'GameController@gameProcess')->name('gameProcess');
Route::post('/save-move', 'GameController@saveMove')->name('saveMove');
