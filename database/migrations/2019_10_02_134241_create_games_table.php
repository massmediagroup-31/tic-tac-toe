<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('players');
            $table->unsignedBigInteger('game_field_id');
            $table->timestamps();

            $table->foreign('id')->references('id')->on('uuids');
            $table->foreign('game_field_id')->references('id')->on('game_fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropForeign(['id']);
            $table->dropForeign(['game_field_id']);
        });

        Schema::dropIfExists('games');
    }
}
